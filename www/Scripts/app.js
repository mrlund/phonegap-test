﻿///#source 1 1 /app/app.js
(function () {
    'use strict';
    
    var app = angular.module('app', [
        // Angular modules 
        'ngAnimate',        // animations
        'ngRoute',          // routing
        'ngSanitize',       // sanitizes html bindings (ex: sidebar.js)

        // Custom modules 
        'common',           // common functions, logger, spinner
        'common.bootstrap', // bootstrap dialog wrapper functions
        'languageFilters',

        // 3rd Party Modules
        'breeze.angular',    // configures breeze for an angular app
        'breeze.directives', // contains the breeze validation directive (zValidate)
        'ui.bootstrap'      // ui-bootstrap (ex: carousel, pagination, dialog)
    ]);
    //Fix to prevent issues using carousel on details page
    angular.module('ui.bootstrap.carousel', ['ui.bootstrap.transition'])
         .controller('CarouselController', ['$scope', '$timeout', '$transition', '$q', function ($scope, $timeout, $transition, $q) {
         }]).directive('carousel', [function () {
             return {

             }
         }]);
    // Handle routing errors and success events
    app.run(['$route',  function ($route) {
            // Include $route to kick start the router.
    }]);

    //var deviceReady = function () {
    //    alert("Booting Angular");
    //    angular.bootstrap(document, ['app']);
    //};
    //if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)) {
    //    alert("Running on PhoneGap!");
    //    window.addEventListener('deviceready', deviceReady, false);
    //} else {
    //    alert("Running in browser!");
    //    window.setTimeout(deviceReady, 500);
    //}


})();
///#source 1 1 /app/config.exceptionHandler.js
// Include in index.html so that app level exceptions are handled.
// Exclude from testRunner.html which should run exactly what it wants to run
(function () {
    'use strict';
    
    var app = angular.module('app');

    // Configure by setting an optional string value for appErrorPrefix.
    // Accessible via config.appErrorPrefix (via config value).

    app.config(['$provide', function ($provide) {
        $provide.decorator('$exceptionHandler',
            ['$delegate', 'config', 'logger', extendExceptionHandler]);
    }]);
    
    // Extend the $exceptionHandler service to also display a toast.
    function extendExceptionHandler($delegate, config, logger) {
        var appErrorPrefix = config.appErrorPrefix;
        var logError = logger.getLogFn('app', 'error');
        return function (exception, cause) {
            $delegate(exception, cause);
            if (appErrorPrefix && exception.message.indexOf(appErrorPrefix) === 0) { return; }

            var errorData = { exception: exception, cause: cause };
            var msg = appErrorPrefix + exception.message;
            logError(msg, errorData, true);
        };
    }
})();
///#source 1 1 /app/config.js
(function () {
    'use strict';

    var app = angular.module('app');

    // Configure Toastr
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';

    //var endpointUrl = "https://rsvpdk.azurewebsites.net/";

    var endpointUrl = "";

    // For use with the HotTowel-Angular-Breeze add-on that uses Breeze
    var remoteServiceName = endpointUrl +'breeze/Breeze';

    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle'
    };

    var config = {
        appErrorPrefix: '[RSVP Error] ', //Configure the exceptionHandler decorator
        docTitle: 'RSVP: ',
        events: events,
        remoteServiceName: remoteServiceName,
        endpointUrl: endpointUrl,
        version: '2.1.0'
    };

    app.value('config', config);
    
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);
    
    //#region Configure the common services via commonConfig
    app.config(['commonConfigProvider', function (cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
        cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
    }]);
    //#endregion
})();
///#source 1 1 /app/config.route.js
(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());

    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({
            redirectTo: function (params, path, search) {
                if (path.indexOf("access_token") !== -1) {
                    var token = queryStringToJSON(path);
                    var expiryMillis = parseInt(token["expires_in"]) * 1000;
                    token[".expires"] = new Date(new Date().getTime() + expiryMillis);
                    localStorage.setItem("token", JSON.stringify(token));
                    return '/register-login';
                }
                return '/';
            }
        });
        function queryStringToJSON(url) {
            var pairs = (url || location.search).slice(1).split('&');
            var result = {};
            for (var idx in pairs) {
                var pair = pairs[idx].split('=');
                if (!!pair[0])
                    result[pair[0].toLowerCase()] = decodeURIComponent(pair[1] || '');
            }
            return result;
        }
    }

    // Define the routes 
    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: 'app/events/events.html',
                    title: 'events',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Events'
                    }
                }
            }, {
                url: '/details/:eventId',
                config: {
                    templateUrl: 'app/details/details.html',
                    title: 'eventDetails',
                    settings: {
                        content: '<i class="fa fa-details"></i> Details'
                    }
                }
            },
            {
                url: '/login',
                config: {
                    templateUrl: 'app/login/login.html',
                    title: 'login',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-login"></i> Login'
                    }
                }
            }, {
                url: '/register-login',
                config: {
                    title: 'register',
                    templateUrl: 'app/register-login/register-login.html',
                    settings: {
                    }
                }
            }
        ];
    }
})();
///#source 1 1 /app/common/common.js
(function () {
    'use strict';

    // Define the common module 
    // Contains services:
    //  - common
    //  - logger
    //  - spinner
    var commonModule = angular.module('common', []);

    // Must configure the common service and set its 
    // events via the commonConfigProvider
    commonModule.provider('commonConfig', function () {
        this.config = {
            // These are the properties we need to set
            //controllerActivateSuccessEvent: '',
            //spinnerToggleEvent: ''
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    });

    commonModule.factory('common',
        ['$q', '$rootScope', '$timeout', 'commonConfig', 'logger', common]);

    function common($q, $rootScope, $timeout, commonConfig, logger) {
        var throttles = {};

        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            $timeout: $timeout,
            // generic
            activateController: activateController,
            createSearchThrottle: createSearchThrottle,
            debouncedThrottle: debouncedThrottle,
            isNumber: isNumber,
            logger: logger, // for accessibility
            textContains: textContains
        };

        return service;

        function activateController(promises, controllerId) {
            return $q.all(promises).then(function (eventArgs) {
                var data = { controllerId: controllerId };
                $broadcast(commonConfig.config.controllerActivateSuccessEvent, data);
            });
        }

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function createSearchThrottle(viewmodel, list, filteredList, filter, delay) {
            // After a delay, search a viewmodel's list using 
            // a filter function, and return a filteredList.

            // custom delay or use default
            delay = +delay || 300;
            // if only vm and list parameters were passed, set others by naming convention 
            if (!filteredList) {
                // assuming list is named sessions, filteredList is filteredSessions
                filteredList = 'filtered' + list[0].toUpperCase() + list.substr(1).toLowerCase(); // string
                // filter function is named sessionFilter
                filter = list + 'Filter'; // function in string form
            }

            // create the filtering function we will call from here
            var filterFn = function () {
                // translates to ...
                // vm.filteredSessions 
                //      = vm.sessions.filter(function(item( { returns vm.sessionFilter (item) } );
                viewmodel[filteredList] = viewmodel[list].filter(function(item) {
                    return viewmodel[filter](item);
                });
            };

            return (function () {
                // Wrapped in outer IFFE so we can use closure 
                // over filterInputTimeout which references the timeout
                var filterInputTimeout;

                // return what becomes the 'applyFilter' function in the controller
                return function(searchNow) {
                    if (filterInputTimeout) {
                        $timeout.cancel(filterInputTimeout);
                        filterInputTimeout = null;
                    }
                    if (searchNow || !delay) {
                        filterFn();
                    } else {
                        filterInputTimeout = $timeout(filterFn, delay);
                    }
                };
            })();
        }

        function debouncedThrottle(key, callback, delay, immediate) {
            // Perform some action (callback) after a delay. 
            // Track the callback by key, so if the same callback 
            // is issued again, restart the delay.

            var defaultDelay = 1000;
            delay = delay || defaultDelay;
            if (throttles[key]) {
                $timeout.cancel(throttles[key]);
                throttles[key] = undefined;
            }
            if (immediate) {
                callback();
            } else {
                throttles[key] = $timeout(callback, delay);
            }
        }

        function isNumber(val) {
            // negative or positive
            return /^[-]?\d+$/.test(val);
        }

        function textContains(text, searchText) {
            return text && -1 !== text.toLowerCase().indexOf(searchText.toLowerCase());
        }
    }
})();
///#source 1 1 /app/common/filters.js
angular.module('languageFilters', []).filter('language', function () {
    return function (input, language) {
        if (input)
        {
            for (i = 0; i < input.length; i++)
            {
                if (input[i].language == language)
                {
                    return input[i];
                }
            }
        }
    };
});
///#source 1 1 /app/common/spinner.js
(function () {
    'use strict';

    // Must configure the common service and set its 
    // events via the commonConfigProvider

    angular.module('common')
        .factory('spinner', ['common', 'commonConfig', spinner]);

    function spinner(common, commonConfig) {
        var service = {
            spinnerHide: spinnerHide,
            spinnerShow: spinnerShow
        };

        return service;

        function spinnerHide() { spinnerToggle(false); }

        function spinnerShow() { spinnerToggle(true); }

        function spinnerToggle(show) {
            common.$broadcast(commonConfig.config.spinnerToggleEvent, { show: show });
        }
    }
})();
///#source 1 1 /app/common/logger.js
(function () {
    'use strict';
    
    angular.module('common').factory('logger', ['$log', logger]);

    function logger($log) {
        var service = {
            getLogFn: getLogFn,
            log: log,
            logError: logError,
            logSuccess: logSuccess,
            logWarning: logWarning
        };

        return service;

        function getLogFn(moduleId, fnName) {
            fnName = fnName || 'log';
            switch (fnName.toLowerCase()) { // convert aliases
                case 'success':
                    fnName = 'logSuccess'; break;
                case 'error':
                    fnName = 'logError'; break;
                case 'warn':
                    fnName = 'logWarning'; break;
                case 'warning':
                    fnName = 'logWarning'; break;
            }

            var logFn = service[fnName] || service.log;
            return function (msg, data, showToast) {
                logFn(msg, data, moduleId, (showToast === undefined) ? true : showToast);
            };
        }

        function log(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'info');
        }

        function logWarning(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'warning');
        }

        function logSuccess(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'success');
        }

        function logError(message, data, source, showToast) {
            logIt(message, data, source, showToast, 'error');
        }

        function logIt(message, data, source, showToast, toastType) {
            var write = (toastType === 'error') ? $log.error : $log.log;
            source = source ? '[' + source + '] ' : '';
            write(source, message, data);
            if (showToast) {
                if (toastType === 'error') {
                    toastr.error(message);
                } else if (toastType === 'warning') {
                    toastr.warning(message);
                } else if (toastType === 'success') {
                    toastr.success(message);
                } else {
                    toastr.info(message);
                }
            }
        }
    }
})();
///#source 1 1 /app/common/bootstrap/bootstrap.dialog.js
(function () {
    'use strict';

    var bootstrapModule = angular.module('common.bootstrap', ['ui.bootstrap']);

    bootstrapModule.factory('bootstrap.dialog', ['$modal', '$templateCache', modalDialog]);

    function modalDialog($modal, $templateCache) {
        var service = {
            deleteDialog: deleteDialog,
            confirmationDialog: confirmationDialog
        };

        $templateCache.put('modalDialog.tpl.html', 
            '<div>' +
            '    <div class="modal-header">' +
            '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-ng-click="cancel()">&times;</button>' +
            '        <h3>{{title}}</h3>' +
            '    </div>' +
            '    <div class="modal-body">' +
            '        <p>{{message}}</p>' +
            '    </div>' +
            '    <div class="modal-footer">' +
            '        <button class="btn btn-primary" data-ng-click="ok()">{{okText}}</button>' +
            '        <button class="btn btn-info" data-ng-click="cancel()">{{cancelText}}</button>' +
            '    </div>' +
            '</div>');

        return service;

        function deleteDialog(itemName) {
            var title = 'Confirm Delete';
            itemName = itemName || 'item';
            var msg = 'Delete ' + itemName + '?';

            return confirmationDialog(title, msg);
        }

        function confirmationDialog(title, msg, okText, cancelText) {

            var modalOptions = {
                templateUrl: 'modalDialog.tpl.html',
                controller: ModalInstance,
                keyboard: true,
                resolve: {
                    options: function () {
                        return {
                            title: title,
                            message: msg,
                            okText: okText,
                            cancelText: cancelText
                        };
                    }
                }
            };

            return $modal.open(modalOptions).result; 
        }
    }

    var ModalInstance = ['$scope', '$modalInstance', 'options',
        function ($scope, $modalInstance, options) {
            $scope.title = options.title || 'Title';
            $scope.message = options.message || '';
            $scope.okText = options.okText || 'OK';
            $scope.cancelText = options.cancelText || 'Cancel';
            $scope.ok = function () { $modalInstance.close('ok'); };
            $scope.cancel = function () { $modalInstance.dismiss('cancel'); };
        }];
})();
///#source 1 1 /app/events/events.js
(function () {
    'use strict';

    var controllerId = 'events';

    // TODO: replace app with your module name
    angular.module('app').controller(controllerId,
        ['common', 'datacontext', '$location', events]);

    function events(common, datacontext, $location) {
        var vm = this;

        vm.activate = activate;
        vm.title = 'events';
        vm.events = [];
        vm.showDetails = showDetails;
        vm.language = "en";
        vm.mediaUrlBase = "http://media.rsvp.dk/mediaitems/";
        vm.isAuthenticated = isAuthenticated;
        vm.imageUrl = imageUrl;

        activate();

        var markers = [];
        var map;
        var query = [
            { id: '1', addr: 'Kronprinssessegade 36, København K', text: 'Bash at the Chez' },
            { id: '2', addr: 'Store Kongensgade 34, København K', text: 'Chez Chick' }
        ];


        function activate() {
            var promises = [getEvents()];
            common.activateController(promises, controllerId)
                .then(function () {
                    //initializeMap();
                    //$.each(query, function (index, item) {
                    //    var count = codeAddress(item.addr, item.id, item.text);
                    //});
                    //window.setTimeout(function(){delayedHoverEvents()}, 500);
                });
        }
        function imageUrl(blobname, thumbnail)
        {
            if (blobname != undefined && blobname != "")
            {
                var url = vm.mediaUrlBase + blobname;
                return thumbnail ? url.replace('.jpg', '-thumb.jpg') : url;
            }
            return "/Images/rsvp-noimage.png";
        }
        function delayedHoverEvents() {
            $(".event-item").on("mouseenter", null, function () {
                var item = $(this).data("id") - 1;
                $(this).addClass("active");
                if (markers[item] != undefined && markers[item].info.opened == false)
                {
                    markers[item].info.open(map, markers[item].mark);
                    markers[item].info.opened = true;
                }
            });
            $(".event-item").on("mouseleave", null, function () {
                var item = $(this).data("id") - 1;
                $(this).removeClass("active");
                if (markers[item] != undefined)
                {
                    markers[item].info.close();
                    markers[item].info.opened = false;
                }
            });
        }
        function getEvents(id) {
            return datacontext.getEvents().then(function (data) {
                return vm.events = data;
            });
        }
        function showDetails(id) {
            $location.path("/events/details/" + id);
            console.log(id);
        }
        function isAuthenticated()
        {
            return datacontext.isAuthenticated();
        }
        /*Google Maps API*/
        var geocoder;


        function initializeMap() {
            geocoder = new google.maps.Geocoder();
            google.maps.InfoWindow.prototype.opened = false;
            var latlng = new google.maps.LatLng(55.684493, 12.581954);
            var map_canvas = document.getElementById('map_canvas');
            var map_options = {
                center: latlng,
                zoom: 13,
                disableDefaultUI: true,
                scrollwheel: false
            }
            map = new google.maps.Map(map_canvas, map_options);
        }

        function codeAddress(address, id, markerText) {
            var image = '/Images/marker.png';
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        icon: image,
                        title: markerText
                    });
                    var infoWindow = new google.maps.InfoWindow({
                        content: "<div>"+ markerText +"</div>"
                    });

                    google.maps.event.addListener(marker, 'mouseover', function () {
                        $(".event-item[data-id=" + id + "]").addClass("active");
                        infoWindow.open(map, marker);
                    });
                    google.maps.event.addListener(marker, 'mouseout', function () {
                        $(".event-item[data-id=" + id + "]").removeClass("active");
                        infoWindow.close();
                    });
                    markers.push({ mark: marker, info: infoWindow });
                    return markers.length;
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
        function wireMapEvents() {
            $("a.event-item").on("mouseover", null, function () {
                var item = $(this).data("id") - 1;
                $(this).addClass("active");
                markers[item].info.open(map, markers[item]);
            });
            $("a.event-item").on("mouseout", null, function () {
                var item = $(this).data("id") - 1;
                $(this).removeClass("active");
                markers[item].info.close();
            });
        }

    }
})();

///#source 1 1 /app/details/details.js
(function () {
    'use strict';

    var controllerId = 'details';

    // TODO: replace app with your module name
    angular.module('app').controller(controllerId,
        ['$scope', '$routeParams', 'datacontext', 'common', '$location', details]);

    function details($scope, $routeParams, datacontext, common, $location) {
        var vm = this;

        vm.activate = activate;
        vm.title = 'details';
        vm.event = {};
        vm.language = "en";
        vm.seatingAvailability = [];
        vm.checkAvailability = checkAvailability;
        vm.requestedSeatsAvailable = false;
        vm.startBooking = startBooking;
        vm.openBooking = openBooking;
        vm.bookingPhase = "none";
        vm.userInfo = {};
        vm.signInOrUp = signInOrUp;
        vm.reservation = {};
        vm.makeBooking = makeBooking;
        vm.makePayment = makePayment;
        vm.requestData = {};
        vm.imageUrl = imageUrl;
        vm.mediaUrlBase = "http://media.rsvp.dk/mediaitems/";
        vm.closeBookingWindow = closeBookingWindow;

        activate();

        function activate() {
            var promises = [getEvents($routeParams.eventId)];
            common.activateController(promises, controllerId)
                .then(function () {
                    if (vm.event.mediaItems.length > 0)
                    {
                        $('.carousel').carousel();
                    }

                    if (datacontext.isAuthenticated()) {
                        datacontext.getUserInfo().then(onSuccess, onFailure);
                    }
                    function onSuccess(userData) {
                        vm.userInfo = userData.results[0];
                        if (vm.userInfo.reservations.length > 0)
                        {
                            $.each(vm.userInfo.reservations, function (idx, item) {
                                if (item.eventId == vm.event.eventId)
                                {
                                    vm.reservation = item;
                                    if (item.isConfirmed === true) {
                                        vm.bookingPhase = "confirmed";
                                    } else {
                                        vm.bookingPhase = "pay";
                                    }
                                }
                            });
                        }
                    }
                    function onFailure(userData)
                    {
                        console.log("Failed");
                    }

                    //log('Activated Dashboard View');
                    $("#myCarousel li").on("click", null, function () {
                        $("#myCarousel li.active").removeClass("active");
                        $(this).addClass("active");
                    });

                    vm.language = datacontext.getLanguage();

                    $(".well").on("change", "#requestedSeats,input.seatingSelector", function () {
                        if (vm.requestedSeatsAvailable == true)
                        {
                            vm.requestedSeatsAvailable = false;
                            $scope.$apply();
                        }
                    });

                });
        }
        function closeBookingWindow() {
            vm.bookingPhase = "none";
        }
        function openBooking()
        {
            vm.bookingPhase = "browse";
        }
        function imageUrl(blobname, thumbnail) {
            if (blobname != undefined && blobname != "") {
                var url = vm.mediaUrlBase + blobname;
                return thumbnail ? url.replace('.jpg', '-thumb.jpg') : url;
            }
            return "/Images/rsvp-noimage.png";
        }
        function makePayment()
        {
            //Dummy payment callback
            //datacontext.dummyPayment(vm.reservation.reservationId).then(function (data) {
            //    datacontext.getUserInfo().then(function (userData) {
            //        vm.userInfo = userData.results[0];
            //    });
            //    vm.bookingPhase = "confirmed";
            //});
            $("#paymentForm").submit();

        }
        function makeBooking()
        {
            console.log("MakeBooking");
            var bookingData = {
                eventId: vm.event.eventId,
                SeatingId: $(".seatingSelector:checked").val(),
                Seats: $("#requestedSeats").val(),
                Comments: $("#comments").val()
            };
            datacontext.makeBooking(bookingData).then(success, fail);
            function success(data)
            {
                var reservation = datacontext.manager.createEntity("Reservation", {
                    reservationId: data.data.ReservationId,
                    seatingId: data.data.SeatingId,
                    userId: data.data.UserId,
                    time: data.data.Time,
                    bookedTime: data.data.BookedTime,
                    seats: data.data.Seats,
                    comments: data.data.Comments,
                    eventId: data.data.EventId,
                    price: data.data.Price,
                    paymentHash: data.data.paymentHash,
                    isConfirmed: data.data.IsConfirmed
                });
                reservation.entityAspect.setUnchanged();
                vm.reservation = reservation;
                vm.userInfo.reservations.push(reservation);
                vm.bookingPhase = reservation.isConfirmed == true ? "confirmed" : "pay";
            }
            function fail(data)
            {
                console.log("Failure");
                console.log(data);
            }
        }
        function signInOrUp()
        {
            var result = datacontext.submitLogin($("#email").val(), $("#password").val()).then(
                function (data) {
                    if (data.status == 200)
                    {
                        console.log("Success");
                        //Get user info
                        datacontext.getUserInfo().then(function (userData) {
                            vm.userInfo = userData.results[0];
                            vm.bookingPhase = "confirm";
                            //TODO: Factor out loop code for reuse above
                            $.each(vm.userInfo.reservations, function (idx, item) {
                                if (item.eventId == vm.event.eventId) {
                                    vm.reservation = item;
                                    if (item.isConfirmed == true) {
                                        vm.bookingPhase = "confirmed";
                                    } else {
                                        vm.bookingPhase = "pay";
                                    }
                                }
                            });
                        });
                        
                    } else {
                        console.log("Fail");
                        //Try registering

                        datacontext.registerUser($("#email").val(), $("#password").val()).then(function () {
                            signInOrUp();
                        });
                    }
                }
            );
        }
        function startBooking()
        {
            //Get user profile, if logged in
            vm.requestData = { start: new Date($(".seatingSelector:checked").data("startdate")), seats: $("#requestedSeats").val() };
            if (datacontext.isAuthenticated())
            {
                vm.bookingPhase = "confirm";
            } else {
                vm.bookingPhase = "signin";
            }
        }
        function checkAvailability()
        {
            datacontext.getAvailability($(".seatingSelector:checked").val(), $("#requestedSeats").val()).then(function (data) {
                vm.requestedSeatsAvailable = data.data == "true" ? true : false;
            });
        }
        function getEvents(id) {
            return datacontext.getEvents(id).then(function (data) {
                return vm.event = data[0];
            });
        }
        function criteriaMatch() {
            return function (item) {
                return item.language === vm.language;
            };
        };

    }
})();

///#source 1 1 /app/layout/shell.js
(function () { 
    'use strict';
    
    var controllerId = 'shell';
    angular.module('app').controller(controllerId,
        ['$rootScope', 'common', 'config', shell]);

    function shell($rootScope, common, config) {
        var vm = this;
        var logSuccess = common.logger.getLogFn(controllerId, 'success');
        var events = config.events;
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.spinnerOptions = {
            radius: 40,
            lines: 7,
            length: 0,
            width: 30,
            speed: 1.7,
            corners: 1.0,
            trail: 100,
            color: '#F58A00'
        };

        activate();

        function activate() {
            logSuccess('Hot Towel Angular loaded!', null, true);
            common.activateController([], controllerId);
        }

        function toggleSpinner(on) { vm.isBusy = on; }

        $rootScope.$on('$routeChangeStart',
            function (event, next, current) { toggleSpinner(true); }
        );
        
        $rootScope.$on(events.controllerActivateSuccess,
            function (data) { toggleSpinner(false); }
        );

        $rootScope.$on(events.spinnerToggle,
            function (data) { toggleSpinner(data.show); }
        );
    };
})();
///#source 1 1 /app/layout/sidebar.js
(function () { 
    'use strict';
    
    var controllerId = 'sidebar';
    angular.module('app').controller(controllerId,
        ['$route', 'config', 'routes', 'datacontext', '$http', sidebar]);

    function sidebar($route, config, routes, datacontext, $http) {
        var vm = this;

        vm.isCurrent = isCurrent;
        vm.userInfo = null;
        vm.myEvents = null;
        vm.language = "en";
        vm.socialLogin = socialLogin;

        activate();

        function activate() { getUserInfo(); getMyEvents(); }
        
        function socialLogin(name) {
            $http.get("/api/Account/ExternalLogins?returnUrl=%2F&generateState=true").then(function (result) {
                $.each(result.data, function (idx, item) {
                    if (name === item.Name) {
                        window.location.href = item.Url;
                    }
                });
            });
        }
        function getUserInfo() {
            if (datacontext.isAuthenticated())
            {
                datacontext.getUserInfo().then(function (data) {
                    vm.userInfo = data.results[0];
                });

            }
        }
        function getMyEvents() {
            if (datacontext.isAuthenticated()) {
                datacontext.getMyEvents().then(function (data) {
                    vm.myEvents = data.data;
                });

            }
        }
        function getNavRoutes() {
            vm.navRoutes = routes.filter(function(r) {
                return r.config.settings && r.config.settings.nav;
            }).sort(function(r1, r2) {
                return r1.config.settings.nav - r2.config.settings.nav;
            });
        }
        
        function isCurrent(route) {
            if (!route.config.title || !$route.current || !$route.current.title) {
                return '';
            }
            var menuName = route.config.title;
            return $route.current.title.substr(0, menuName.length) === menuName ? 'current' : '';
        }
    };
})();

///#source 1 1 /app/login/login.js
(function () {
    'use strict';

    var controllerId = 'login';

    // TODO: replace app with your module name
    angular.module('app').controller(controllerId,
        ['$scope', 'datacontext', '$location','common', login]);

    function login($scope, datacontext, $location, common) {
        var vm = this;

        vm.activate = activate;
        vm.title = 'login';
        vm.username = "ml@eqt1.com";
        vm.password = "Access";
        vm.submitLogin = submitLogin;
        vm.succeeded = false;

        function activate() { }

        function submitLogin() {

            var result = datacontext.submitLogin(vm.username, vm.password).then(function (data) {
                if (data.status == 200)
                {
                    vm.succeeded = true;
                    redirect();
                } else {
                    vm.succeeded = false;
                }
                
            });
        }
        function redirect() {
            $location.path("/");
        }
    }
})();

///#source 1 1 /app/register-login/register-login.js
(function () {
    'use strict';

    var controllerId = 'register_login';

    // TODO: replace app with your module name
    angular.module('app').controller(controllerId,
        ['$scope','authentication','$http', '$location', register_login]);

    function register_login($scope, authentication, $http, $location) {
        var vm = this;
        var provider = "";
        vm.activate = activate;
        vm.title = 'register_login';
        vm.registerUser = registerUser;
        vm.email = "";

        activate();

        function activate() {
            if (authentication.isAuthenticated())
            {
                var getConfig = { headers: { "Authorization": "Bearer " + authentication.getToken() } };
                $http.get("/api/account/userinfo", getConfig).then(function (data) {
                    if (data.data.HasRegistered === true)
                    {
                        $location.path("/");
                    } else {
                        provider = data.data.LoginProvider;
                        vm.email = data.data.Email;
                    }
                });
            }
        }
        function registerUser() {
            var postdata = "Email=" + encodeURIComponent(vm.email);
            var config = { headers: { "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Bearer " + authentication.getToken() } };
            return $http.post("/api/account/registerexternal", postdata, config).then(success, failed);

            function success() {
                var url = "/api/Account/ExternalLogin?provider="+  provider +"&response_type=token&client_id=self&redirect_uri=https%3A%2F%2Fwww.rsvp.dk%2F&state=" + authentication.getState();
                window.location.href = url;
            }
            function failed() {
                console.log("Failed");
            }
        }
    }
})();

///#source 1 1 /app/services/authentication.js
(function () {
    'use strict';

    var serviceId = 'authentication';

    // TODO: replace app with your module name
    angular.module('app').factory(serviceId, [authentication]);

    function authentication() {
        var protectedControllers = [
            "breeze/Breeze/UserInfo",
            "breeze/Breeze/MyEvents"
        ];
        var token = null;
        // Define the functions and properties to reveal.
        var service = {
            protectedControllers: protectedControllers,
            getToken: getToken,
            isAuthenticated: isAuthenticated,
            getState: getState
        };

        return service;

        function isAuthenticated() {
            if (token == undefined || token == null || token.access_token == undefined) {
                var storedToken = localStorage.getItem("token");
                if (storedToken != null) {
                    token = JSON.parse(storedToken);
                }
            }
            return token !== null && token.access_token !== undefined && new Date() < new Date(token[".expires"]);
        }

        function getToken() {
            return token.access_token;
        }
        function getState() {
            isAuthenticated();
            return token.state;
        }

        //#region Internal Methods        

        //#endregion
    }
})();
///#source 1 1 /app/services/datacontext.js
(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['common', 'entityManagerFactory', 'authentication', '$http', 'config', datacontext]);

    function datacontext(common, entityManagerFactory, authentication, $http, config) {
        var $q = common.$q;

        var manager = entityManagerFactory.newManager();
        var EntityQuery = breeze.EntityQuery;
        var language = "en"; //TODO: Implement language switching
        var userInfo = null;
        var eventsLoadtime = new Date();

        var service = {
            getEvents: getEvents,
            isAuthenticated: isAuthenticated,
            submitLogin: submitLogin,
            createEvent: createEvent,
            manager: manager,
            getLanguage: getLanguage,
            getVenues: getVenues,
            getChefs: getChefs,
            saveChanges: saveChanges,
            getMyEvents: getMyEvents,
            getAvailability: getAvailability,
            getUserInfo: getUserInfo,
            makeBooking: makeBooking,
            registerUser: registerUser
        };

        return service;

        function makeBooking(bookingData) {
            var postConfig = { headers: { "Authorization": "Bearer " + authentication.getToken() } };
            return $http.post(config.endpointUrl + "/api/Events/" + bookingData.eventId + "/Reservations", bookingData, postConfig);
        }
        function getUserInfo() {
            if (userInfo == null) {
                var query = EntityQuery.from("UserInfo").expand("Address,Reservations.Invoice,Reservations.Seating.Event");
                return manager.executeQuery(query).then(function (data) {
                    userInfo = data;
                    return data;
                });
            } else {
                return $q.when(userInfo);
            }

        }
        function getAvailability(seatingId, seatCount) {
            return $http.get(config.endpointUrl + "/api/Seating/" + seatingId + "/CheckAvailability/" + seatCount)
        }
        function getMyEvents() {
            var postConfig = { headers: { "Authorization": "Bearer " + authentication.getToken() } };
            return $http.post(config.endpointUrl + "/api/MyEvents", null, postConfig);
        }
        function getChefs() {
            var query = EntityQuery.from("Chefs").expand("Descriptions");
            return manager.executeQuery(query);
        }

        function getVenues() {
            var query = EntityQuery.from("Venues").expand("Address");
            return manager.executeQuery(query);
        }

        function getEvents(id) {

            if (id != undefined) {
                var query = EntityQuery.from("Events").where("eventId", "==", id).expand("Chef.Descriptions,Menu.Courses.Descriptions,Venue.Address,Links,MediaItems,Seatings,Descriptions");
            } else {
                var query = EntityQuery.from("Events").expand("Chef.Descriptions,Menu.Courses.Descriptions,Venue.Address,Links,MediaItems,Seatings,Descriptions");
            }
            var cacheAge = new Date().getTime() - eventsLoadtime.getTime();
            if (!manager.metadataStore.isEmpty() && cacheAge < 120000) {
                var cacheResult = manager.executeQueryLocally(query);
            }
            if (cacheResult) {
                if (id != undefined || cacheResult.length > 1) {
                    return $q.when(cacheResult);
                }
            }
            return manager.executeQuery(query).then(success);

            function success(data) {
                eventsLoadtime = new Date();
                console.log("Retrieved " + data.results.length);
                return data.results;
            }
        }

        function registerUser(username, password) {
            var regData = { Email: username, Password: password, ConfirmPassword: password };
            return $http.post(config.endpointUrl + "api/account/register", regData);
        }

        function createEvent() {
            if (manager.metadataStore.isEmpty()) {
                manager.fetchMetadata().then(function () {
                    return manager.createEntity('Event');
                });

            } else {
                return manager.createEntity('Event');
            }
        }
        function getLanguage() {
            return language;
        }

        function saveChanges() {
            if (manager.hasChanges()) {
                var result = manager.saveChanges()
                    .then(function (data) {
                        console.log("Saved");
                    });
                return result;
            } else {
                console.log("Nothing to save");
                return $q.when(false);
            };
        };
        function canEdit(eventId) {
            if (datacontext.isAuthenticated()) {
                var event = datacontext.manager.getEntityByKey("Event", eventId);
                //TODO: Send userId with token. Is this the right thing to do? How do we securely seperate client/server validation?
                if (event.chefId == token.userId) {
                    return true;
                }
            }
            return false;
        }

        function submitLogin(username, password) {
            var postdata = "grant_type=password&" + "username=" + username + "&password=" + password;
            var postConfig = { headers: { "Content-Type": "application/x-www-form-urlencoded" } };
            return $http.post(config.endpointUrl + "token", postdata, postConfig).then(success, failed);

            function success(data) {
                authentication.token = data.data;
                localStorage.setItem("token", JSON.stringify(authentication.token));
                return $q.when(data);
            }
            function failed(data) {
                return $q.when(data);
            }

        }
        function isAuthenticated() {
            return authentication.isAuthenticated();
        }
    }
})();
///#source 1 1 /app/services/directives.js
(function() {
    'use strict';

    var app = angular.module('app');

    app.directive('ccImgPerson', ['config', function (config) {
        //Usage:
        //<img data-cc-img-person="{{s.speaker.imageSource}}"/>
        var basePath = config.imageSettings.imageBasePath;
        var unknownImage = config.imageSettings.unknownPersonImageSource;
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            attrs.$observe('ccImgPerson', function(value) {
                value = basePath + (value || unknownImage);
                attrs.$set('src', value);
            });
        }
    }]);


    app.directive('ccSidebar', function () {
        // Opens and clsoes the sidebar menu.
        // Usage:
        //  <div data-cc-sidebar>
        // Creates:
        //  <div data-cc-sidebar class="sidebar">
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            var $sidebarInner = element.find('.sidebar-inner');
            var $dropdownElement = element.find('.sidebar-dropdown a');
            element.addClass('sidebar');
            $dropdownElement.click(dropdown);

            function dropdown(e) {
                var dropClass = 'dropy';
                e.preventDefault();
                if (!$dropdownElement.hasClass(dropClass)) {
                    hideAllSidebars();
                    $sidebarInner.slideDown(350);
                    $dropdownElement.addClass(dropClass);
                } else if ($dropdownElement.hasClass(dropClass)) {
                    $dropdownElement.removeClass(dropClass);
                    $sidebarInner.slideUp(350);
                }

                function hideAllSidebars() {
                    $sidebarInner.slideUp(350);
                    $('.sidebar-dropdown a').removeClass(dropClass);
                }
            }
        }
    });


    app.directive('ccWidgetClose', function () {
        // Usage:
        // <a data-cc-widget-close></a>
        // Creates:
        // <a data-cc-widget-close="" href="#" class="wclose">
        //     <i class="fa fa-remove"></i>
        // </a>
        var directive = {
            link: link,
            template: '<i class="fa fa-remove"></i>',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            attrs.$set('href', '#');
            attrs.$set('wclose');
            element.click(close);

            function close(e) {
                e.preventDefault();
                element.parent().parent().parent().hide(100);
            }
        }
    });

    app.directive('ccWidgetMinimize', function () {
        // Usage:
        // <a data-cc-widget-minimize></a>
        // Creates:
        // <a data-cc-widget-minimize="" href="#"><i class="fa fa-chevron-up"></i></a>
        var directive = {
            link: link,
            template: '<i class="fa fa-chevron-up"></i>',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            //$('body').on('click', '.widget .wminimize', minimize);
            attrs.$set('href', '#');
            attrs.$set('wminimize');
            element.click(minimize);

            function minimize(e) {
                e.preventDefault();
                var $wcontent = element.parent().parent().next('.widget-content');
                var iElement = element.children('i');
                if ($wcontent.is(':visible')) {
                    iElement.removeClass('fa fa-chevron-up');
                    iElement.addClass('fa fa-chevron-down');
                } else {
                    iElement.removeClass('fa fa-chevron-down');
                    iElement.addClass('fa fa-chevron-up');
                }
                $wcontent.toggle(500);
            }
        }
    });

    app.directive('ccScrollToTop', ['$window',
        // Usage:
        // <span data-cc-scroll-to-top></span>
        // Creates:
        // <span data-cc-scroll-to-top="" class="totop">
        //      <a href="#"><i class="fa fa-chevron-up"></i></a>
        // </span>
        function ($window) {
            var directive = {
                link: link,
                template: '<a href="#"><i class="fa fa-chevron-up"></i></a>',
                restrict: 'A'
            };
            return directive;

            function link(scope, element, attrs) {
                var $win = $($window);
                element.addClass('totop');
                $win.scroll(toggleIcon);

                element.find('a').click(function (e) {
                    e.preventDefault();
                    // Learning Point: $anchorScroll works, but no animation
                    //$anchorScroll();
                    $('body').animate({ scrollTop: 0 }, 500);
                });

                function toggleIcon() {
                    $win.scrollTop() > 300 ? element.slideDown(): element.slideUp();
                }
            }
        }
    ]);

    app.directive('ccSpinner', ['$window', function ($window) {
        // Description:
        //  Creates a new Spinner and sets its options
        // Usage:
        //  <div data-cc-spinner="vm.spinnerOptions"></div>
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
            scope.spinner = null;
            scope.$watch(attrs.ccSpinner, function (options) {
                if (scope.spinner) {
                    scope.spinner.stop();
                }
                scope.spinner = new $window.Spinner(options);
                scope.spinner.spin(element[0]);
            }, true);
        }
    }]);

    app.directive('ccWidgetHeader', function() {
        //Usage:
        //<div data-cc-widget-header title="vm.map.title"></div>
        var directive = {
            link: link,
            scope: {
                'title': '@',
                'subtitle': '@',
                'rightText': '@',
                'allowCollapse': '@'
            },
            templateUrl: '/app/layout/widgetheader.html',
            restrict: 'A',
        };
        return directive;

        function link(scope, element, attrs) {
            attrs.$set('class', 'widget-head');
        }
    });
})();
///#source 1 1 /app/services/entityManagerFactory.js
(function() {
    'use strict';
    
    var serviceId = 'entityManagerFactory';
    angular.module('app').factory(serviceId, ['breeze', 'config', 'authentication', emFactory]);

    function emFactory(breeze, config, authentication) {
        // Convert server-side PascalCase to client-side camelCase property names
        breeze.NamingConvention.camelCase.setAsDefault();

        // Add auth token to requests requiring authentication
        var ajaxImpl = breeze.config.getAdapterInstance('ajax');
        ajaxImpl.requestInterceptor = function (requestInfo) {
            if (authentication.protectedControllers.some(function (item) {
                return (new RegExp(item)).test(requestInfo.config.url);
            })) {
                if (authentication.isAuthenticated()) {
                    requestInfo.config.headers = { "Authorization": "Bearer " + authentication.getToken() };
                }
            }
        }

        // Do not validate when we attach a newly created entity to an EntityManager.
        // We could also set this per entityManager
        new breeze.ValidationOptions({ validateOnAttach: false }).setAsDefault();
        
        //Modify entity type definition to include calculated property
        //This property is sent camelcase from the server in order to modelbind. Set JsonPropertyName annotation on model.
        //function Seating() {
        //    this.remainingSeats = 0;
        //}
        //store.registerEntityTypeCtor('Seating', Seating);


        var serviceName = config.remoteServiceName;
        var metadataStore = new breeze.MetadataStore();

        var provider = {
            metadataStore: metadataStore,
            newManager: newManager
        };
        
        return provider;

        function newManager() {
            var mgr = new breeze.EntityManager({
                serviceName: serviceName,
                metadataStore: metadataStore
            });
            return mgr;
        }

    }
})();
